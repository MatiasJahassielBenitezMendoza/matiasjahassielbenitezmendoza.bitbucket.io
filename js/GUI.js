var GUI = (function(){

var albumes = [];


var init = function(){
  albumes.push("Juquila2015");
  albumes.push("Figuras");
  albumes.push("Mexico");
  albumes.push("Otro");
  Componentes.agregar_album("1","pb-wrapper pb-wrapper-1", "lightbox[album1]" ,
  ["https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FJuquila2015%2F1.JPG?alt=media&token=95d4a51f-869c-4d83-a485-9bb1c3db8cff",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FJuquila2015%2F2.JPG?alt=media&token=cb1dc048-eed3-4caa-8c7f-9405b6d5d216",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FJuquila2015%2F3.JPG?alt=media&token=efb631b4-5c76-4924-a5ec-2d1879bb95d0",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FJuquila2015%2F4.JPG?alt=media&token=de8da77a-d9b6-4fbb-8e3f-24d34f739d60",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FJuquila2015%2F5.JPG?alt=media&token=c31c98e4-b567-42fa-878e-c6ae3a6adec0"
],albumes[0]);
  Componentes.agregar_album("2","pb-wrapper pb-wrapper-2", "lightbox[album2]" ,
  ["https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FFiguras%2F1.jpg?alt=media&token=9bdb7890-81ad-45d5-abfa-805ff87b0b17",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FFiguras%2F2.jpg?alt=media&token=f1f8a5b4-4252-40a6-91e8-b35305da4f9e",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FFiguras%2F3.jpg?alt=media&token=af704d8d-4f4a-470d-8a0b-c2be3e024508",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FFiguras%2F4.jpg?alt=media&token=fed21f0e-04e0-4576-b62b-440ce58afe79",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FFiguras%2F5.jpg?alt=media&token=bb317448-2aa2-426b-84d2-5b9bcb2be835",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FFiguras%2F6.jpg?alt=media&token=789d57ab-eb4f-43ad-b924-405b12070e20",
  "https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FFiguras%2F7.jpg?alt=media&token=412e58f8-9aed-4600-bff3-60a3a9328d99"
],albumes[1]);
Componentes.agregar_album("3","pb-wrapper pb-wrapper-3", "lightbox[album3]" ,
["https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FMexico%2F1.jpg?alt=media&token=3f74cfc2-a1e9-4185-9174-7e987755c17a",
"https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FMexico%2F2.jpg?alt=media&token=2b163535-0b88-49b2-bc5f-0b4f72daea0a",
],albumes[2]);

Componentes.agregar_album("4","pb-wrapper pb-wrapper-4", "lightbox[album4]" ,
["https://firebasestorage.googleapis.com/v0/b/matias-benitez.appspot.com/o/Imagenes%2FOtro%2F1.jpg?alt=media&token=31b64f23-1200-496e-a90e-2f2c22926c50"],
albumes[3]);

}


var agregar = function(al, img){
var id = 0;
for(var i=0; i<albumes.length; i++){
  if(al === albumes[i]){
    id = i+1;
  }
}

if(id === 0){
  console.log("Debes ingresar un album valido");
}
else{
  Componentes.agregar_foto(id,al,img);
  console.log("Se agrego a la vista");
}


}

return{
      "agregar": agregar,
      "init":init
     }
})();
