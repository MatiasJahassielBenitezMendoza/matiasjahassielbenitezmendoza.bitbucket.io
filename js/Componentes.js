var Componentes = (function(){


var agregar_album = function(id,clase, album, fotos, nom){
  var div = document.createElement("div");
  div.setAttribute("class",clase);
  var div2 = document.createElement("div");
  div2.setAttribute("class","pb-scroll");
  var ul = document.createElement("ul");
  ul.setAttribute("class","pb-strip");
  ul.setAttribute("id",id);
  var h3 = document.createElement("h3");
  h3.setAttribute("class","pb-title");
  h3.textContent = nom;

  for (var i=0; i<fotos.length; i++){
     var li = document.createElement("li");
     var a = document.createElement("a");
     a.setAttribute("href",fotos[i]);
     a.setAttribute("rel",album);
     var img = document.createElement("img");
     img.setAttribute("src",fotos[i]);
     img.setAttribute("width","150px");
     img.setAttribute("height","160px");
     a.appendChild(img);
     li.appendChild(a);
     ul.appendChild(li);
   }
  div2.appendChild(ul);
  div.appendChild(div2);
  div.appendChild(h3);

 var seccion = document.getElementById("main")
 seccion.appendChild(div);

 };


 var agregar_foto = function(id, album,foto){
     var alb = document.getElementById(id);
     var li = document.createElement("li")
      var a = document.createElement("a")
      a.setAttribute("href",foto);
      a.setAttribute("rel",album);
      var img = document.createElement("img")
      img.setAttribute("src",foto);
      img.setAttribute("width","150px");
      img.setAttribute("height","160px");
      a.appendChild(img);
      li.appendChild(a);
      alb.appendChild(li);
  };


   return {
           "agregar_album": agregar_album,
           "agregar_foto": agregar_foto
         };

   })();
